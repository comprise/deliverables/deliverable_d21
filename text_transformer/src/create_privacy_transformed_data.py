#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  7 10:31:38 2019

@author: didelani
"""
import os
import sys
import numpy as np
from collections import defaultdict, Counter
import pandas as pd
import re
import json


data_dir = '../data/training/bio_cased/'
output_dir = '../data/training/'

def extract_sentences(data_dir):
    data_path = data_dir+'train.tsv'
    with open(data_path) as f:
        docs = f.readlines()
    
    sentences = []
    n_sent = 0
    sent= []
    list_sent_len = []
    
    named_entities = defaultdict(lambda: defaultdict(int))
    multi_word_entites = defaultdict(lambda: defaultdict(int))
    sent_per = 0
    print(len(docs))
    for i, line in enumerate(docs):
        if i == 0:
            continue
        if len(line) < 3:
            sentences.append(sent)
            sent = []
            list_sent_len.append(sent_per)
            sent_per = 0
            n_sent += 1
        else:
            token, ne = line.strip().split('\t') 
            sent.append((token, ne))
            named_entities[ne][token]+=1
               
            k = i
            new_token = token + ' '
            while k+1 < len(docs) and len(docs[k+1]) > 3 and ne!='O' and ne[:2]=='B-':
                
                n_tok, n_ne = docs[k+1].strip().split('\t') 
            
                if ne[2:] == n_ne[2:] and n_ne[:2] == 'I-':
                    new_token+= n_tok + ' '
                    k+=1
                else:
                    break
                    
            sent_per+=1
            
            if len(new_token) > len(token)+1:
                named_entities['MULTI-WORD_'+ne[2:]][new_token]+=1  
                    
    print('# of sentences ', len(sentences))
    
    print('# multi-word entity categories', len(multi_word_entites))
    for ne in named_entities:
        if 'MULTI-WORD' in ne:
            print(ne, named_entities[ne])
    
    
    return sentences, list_sent_len, named_entities, multi_word_entites

def sort_named_entities_by_freq(transf_dir, named_entities):
    
    named_entities_freq = []

    named_entity_to_idx = dict()
    
    k = 0
    for named_entity in named_entities:
        ne_dict = Counter(named_entities[named_entity])
        total_sum = sum(ne_dict.values())
    
        # compute the probability of ne in corpus
        prob_ne = [[name, val, val/total_sum] for name, val in ne_dict.items()]
        nes = pd.DataFrame(prob_ne, columns=[named_entity, 'count', 'probability'])
        nes = nes.sort_values('probability', ascending=False)
        nes = nes.reset_index(drop=True)
        
        named_entities_freq.append(nes)
        named_entity_to_idx[named_entity]=k
        k+=1
        print(nes.head())
        
        nes.to_csv(transf_dir+named_entity+'.tsv', sep='\t', index=False)
        
    with open(transf_dir+'named_entity_to_idx.json', 'w') as f:
        json.dump(named_entity_to_idx, f)
        
    return named_entities_freq, named_entity_to_idx


def check_multiword(sent_o):
    sent_new = []
    n = len(sent_o)
    i=0
    while i < n:
        word_feat = sent_o[i]
        if word_feat[1] != 'O':
            j=i
            act_label = word_feat[1][2:]
            n_first_ne = word_feat[0]
            word_feat = (n_first_ne, word_feat[1])
            while j+1 < n and sent_o[j+1][1][2:] == act_label:
                word_feat = (n_first_ne, word_feat[1])
                j+=1
            i=j
        sent_new.append(word_feat)
        i+=1
    return sent_new

def anonymize_corpus_placeholder(sentences, multi_word = False):
    
    sentences_ph = []
    new_sentences_ph = []
    for j, sent in enumerate(sentences):
        sent_ph = []
        new_sent = []
        
        if multi_word:
            sent = check_multiword(sent)
            
        for k, word_label in enumerate(sent):
            token, ne = word_label
            
            N_words = len(sent)
            new_token = token
            if ne!='O': #and ne_Counts[token] > threshold:
                new_token = 'PLACEHOLDER'
            else:
                new_token = token
                            
            new_sent.append((new_token, ne))
            sent_ph.append((token, ne))
            
        sentences_ph.append(sent_ph)
        new_sentences_ph.append(new_sent)

    # print an example of anonymized sentence
    kk = 8816
    conll_Sent = ''
    for token, ne in sentences_ph[kk]:
        conll_Sent += token +' ' 
    print('Text sample sentence: \n', conll_Sent)

    anonymous_sent = ''
    for token, ne in new_sentences_ph[kk]:
        anonymous_sent += token +' ' 
    print('Text anonymized sentence: \n', anonymous_sent)
    
    return new_sentences_ph


def anonymize_corpus_same_type(list_sent_len, sentences, ne_table, ne_label='B-TIME', multi_word=False):
    N_words = np.max(list_sent_len)
    N_sents = len(sentences)
    
    N_popNE = ne_table.shape[0]
    sel_ne_ids = np.random.choice(range(N_popNE), (N_sents, N_words), p=ne_table['probability'].values)
    ne_list = list(ne_table[ne_label].values)

    new_sentences = []
    anony_ne_map = defaultdict(list)
    for j, sent in enumerate(sentences):
        new_sent = []
        #print(sent)
        per_NE = dict()
        per_no = 0
        
        if multi_word:
            sent = check_multiword(sent)
            
        for k, word_label in enumerate(sent):
            token, ne = word_label
            N_words = len(sent)
            new_token = token
            if ne == ne_label:
                if token not in per_NE:
                    ne_idx =  sel_ne_ids[j][per_no]
                    new_token = ne_list[ne_idx]
                    per_no += 1
                    per_NE[token] = new_token
                else:
                    new_token = per_NE[token]
                #print(token, new_token)
                anony_ne_map[new_token].append((token, j, k))
                
            new_sent.append((new_token, ne))

        new_sentences.append(new_sent)

    # print an example of anonymized sentence

    kk = 8816
    conll_Sent = ''
    for token, ne in sentences[kk]:
        conll_Sent += token +' ' 
    print('Text sample sentence: \n', conll_Sent)

    anonymous_sent = ''
    for token, ne in new_sentences[kk]:
        anonymous_sent += token +' ' 
    print('Text anonymized sentence: \n', anonymous_sent)
    
    return new_sentences


def tranform_text_and_save(sentences, list_sent_len, sorted_ne_freq, ne_to_idx, placeholder=True, multi_word=False):
    
    folder_name = ''
    if placeholder:
        if multi_word:
            anony_sentences = anonymize_corpus_placeholder(sentences, multi_word=True)
            folder_name = 'placeholder_multiword'
        else:
            anony_sentences = anonymize_corpus_placeholder(sentences)
            folder_name = 'placeholder_singleword'
    else:
        
        anony_sentences = sentences
        for ne in ne_to_idx:
            ne_freq = sorted_ne_freq[ne_to_idx[ne]]
            if 'MULTI-WORD' not in ne and ne!='O':
                print(ne)
                if  multi_word:
                    anony_sentences = anonymize_corpus_same_type(list_sent_len, anony_sentences, ne_freq, ne, multi_word=True)
                    folder_name = 'sametype_multiword'
                else: 
                    anony_sentences = anonymize_corpus_same_type(list_sent_len, anony_sentences, ne_freq, ne)
                    folder_name = 'sametype_singleword'
                    
    new_output_dir = output_dir+folder_name+'/'
    if not os.path.exists(new_output_dir):
        os.makedirs(new_output_dir)
    anony_df = pd.DataFrame(anony_sentences)
    anony_df.to_csv(new_output_dir+'train.tsv', sep='\t', header=False, index=False)
    
    valid_df = pd.read_csv(data_dir+'valid.tsv', sep='\t', header=None)
    valid_df.to_csv(new_output_dir+'valid.tsv', sep='\t', header=False, index=False)
    
    
    test_df = pd.read_csv(data_dir+'test.tsv', sep='\t', header=None)
    test_df.to_csv(new_output_dir+'test.tsv', sep='\t', header=False, index=False)
    
     
if __name__ == '__main__':
    
    # Read the NER data using spaces as separators, keeping blank lines and adding columns
    
    ner_data = pd.read_csv(data_dir+'train.tsv', sep="\t", header=None, encoding="utf-8")
    ner_data.columns = ["token", "ne"]
    # Explore the distribution of NE tags in the dataset
    tag_distribution = ner_data.groupby("ne").size().reset_index(name='counts')
    print("Number of tokens per named entity: ")
    print(tag_distribution)
    
    sentences, list_of_sentence_len, named_entities, multi_word_entites = extract_sentences(data_dir)
    print(sentences[8816])
    '''
    for k in range(0, len(sentences)):
        anonymous_sent = ''
        tokens = []
        for token, ne in sentences[k]:
            anonymous_sent += token +' ' 
            tokens.append(token.lower())
        if 'york' in tokens or 'frankfurt' in tokens or 'fransisco' in tokens:
            print(k, anonymous_sent)
    '''
    
    transf_dir = '../data/transformation/'
    if not os.path.exists(transf_dir):
        os.makedirs(transf_dir)
    sorted_ne_freq, ne_to_idx = sort_named_entities_by_freq(transf_dir, named_entities)
    
    
    tranform_text_and_save(sentences, list_of_sentence_len, sorted_ne_freq, ne_to_idx, placeholder=True, multi_word=False)
    tranform_text_and_save(sentences, list_of_sentence_len, sorted_ne_freq, ne_to_idx, placeholder=True, multi_word=True)
    tranform_text_and_save(sentences, list_of_sentence_len, sorted_ne_freq, ne_to_idx, placeholder=False, multi_word=False)
    tranform_text_and_save(sentences, list_of_sentence_len, sorted_ne_freq, ne_to_idx, placeholder=False, multi_word=True)
    

    
