#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  8 10:59:22 2019

@author: didelani
"""

import os
import pandas as pd
import numpy as np
import argparse
import sys
import json
from flair.data import Sentence
from flair.models import SequenceTagger
from flair.data import Corpus
from flair.datasets import ColumnCorpus
from flair.embeddings import TokenEmbeddings, WordEmbeddings, StackedEmbeddings
from typing import List
from flair.trainers import ModelTrainer
from flair.visual.training_curves import Plotter


model = SequenceTagger.load('resources/taggers/cased-ner/best-model.pt')

def predict_ner_output_for_sentence(sentr):    
    
    sentence = Sentence(sentr.strip())

    # predict tags and print
    model.predict(sentence)
    tagged_sent = sentence.to_tagged_string()
        
    coNLL_format_tags = []
    
    tagged_words = tagged_sent.split()
    for k, tagged_word in enumerate(tagged_words):
        token = tagged_word
        if token.startswith('<') and token.endswith('>'):
            continue
        
        if k < len(tagged_words)-1:
            next_token = tagged_words[k+1]
        else:
            next_token = ''
        
        if next_token.startswith('<') and next_token.endswith('>'):
            tag = next_token[1:-1]
        else:
            tag = 'O'
        coNLL_format_tags.append([token, tag])
        
        
        
    print(coNLL_format_tags)
    return tagged_sent, coNLL_format_tags

def anonymize_sentence_singleword(sent, ne_table_list, nes_to_idxs):
    N_words = 50
    
    for ne_label, idx in nes_to_idxs.items():
        if ne_label == 'O': continue
        ne_table = ne_table_list[idx]
        N_popNE = ne_table.shape[0]
        sel_ne_ids = np.random.choice(range(N_popNE), (1, N_words), p=ne_table['probability'].values)
        sel_ne_ids = sel_ne_ids.flatten()
        ne_list = list(ne_table[ne_label].values)

        new_sent = []
        #print(sent)
        per_NE = dict()
        per_no = 0
        for k, word_label in enumerate(sent):
            token, ne = word_label
            N_words = len(sent)
            new_token = token
            if ne == ne_label:
                if token not in per_NE:
                    ne_idx =  sel_ne_ids[per_no]
                    new_token = ne_list[ne_idx]
                    per_no += 1
                    per_NE[token] = new_token
                else:
                    new_token = per_NE[token]
                #print(token, new_token)
            new_sent.append((new_token, ne))

        sent = new_sent
    
    new_sent =  sent
    anonymous_sent = ''
    for token, ne in new_sent:
        anonymous_sent += token +' ' 
    #print('Text anonymized sentence: \n', anonymous_sent)
    
    return anonymous_sent


    
if __name__ == '__main__':
    
    tranf_dir = '../data/transformation/'
    with open(tranf_dir+'named_entity_to_idx.json') as f:
        nes_to_idxs = json.load(f)
        
    ne_table_list = ['' for _ in range(len(nes_to_idxs))]
    for ne, idx in nes_to_idxs.items():
        ne_df = pd.read_csv(tranf_dir+ne+'.tsv', sep='\t')
        ne_table_list[idx] = ne_df
        
    print('Enter "exit" for sample sentence to quit ')
    sent = ''
    while (sent!='exit'):
        sent = input('Enter a sample sentence: ')    
        tagged_sent, coNLL_word_tag = predict_ner_output_for_sentence(sent)
        print("Tagged sentence: ", tagged_sent)
         
        replace_type = input('Enter 0 for word by word NE replacement or 1 for multiword replacement ') 
        new_sentence = anonymize_sentence_singleword(coNLL_word_tag, ne_table_list, nes_to_idxs)
        print("Transformed sentence: ", new_sentence)