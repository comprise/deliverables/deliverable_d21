#!/usr/bin/env python

# This file is a part of the word masking tool
# developed as part of the COMPRISE project
# Author(s): Imran Sheikh
# Copyright © 2019 INRIA
#
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

import os.path

def verbmobil_eng_id2wav(id, cd_suffix='.1', corpus_path='/talc/multispeech/corpus/dialog/Verbmobil/Verbmobil/'):
	spkr, dialog, utt, version, part, cd = id.split('_') # MIMVMX_q002nxx0_022_130020_VM1_06
	cdDir = 'VM'+str(int(cd)) + cd_suffix
	dialogDir = dialog[0:5]
	spkr = spkr[0:3]
	if dialog[0] == 'm':
		wavfile = dialog+'_'+utt+'_'+spkr+'_ENG.wav'
	else:
		wavfile = dialog+'_'+utt+'_'+spkr+'.wav'

	wavpath = os.path.join(corpus_path, cdDir, 'data', dialogDir, wavfile)

	if not os.path.isfile(wavpath):
		raise Exception('Verbmobil wav file does not exist: ' + wavpath)

	return wavpath

def verbmobil_eng_id2ctm(id, ctm_path):
	ctmfile = id+'.ctm'
	ctmpath = os.path.join(ctm_path, ctmfile)

	if not os.path.isfile(ctmpath):
		ctmpath = None

	return ctmpath
