#!/usr/bin/env python

# This file is a part of the word masking tool
# developed as part of the COMPRISE project
# Author(s): Imran Sheikh
# Copyright © 2019 INRIA
#
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

import numpy
from scipy.io import wavfile

class WavRegionMasker:
	def __init__(self, mode="silence"):
		self.mode = mode
		if self.mode == "silence":
			self.mask_regions_in_wav = self.silence_regions_in_wav
		else:
			raise Exception('Unsupported masking mode: ' + mode)

	def seconds2samples(self, mask_regions, fs):
		return [[int(t * fs) for t in region] for region in mask_regions]

	def validateRegions(self, mask_regions):
		for region in mask_regions:
			if region[1] <= region[0]:
				raise Exception('Exception in mask_regions: ' + mask_regions)

	def silence_regions_in_wav(self, inwavfile, mask_regions, outwavfile):
		fs, data = wavfile.read(inwavfile)
		wavdata = numpy.array(data)
		if wavdata.dtype == numpy.int16:
			silenceValue = numpy.int16(0)
		elif wavdata.dtype == numpy.float32:
			silenceValue = numpy.float32(0.0)
		else:		
			raise Exception('Unsupported wav sample format: ' + data[1].dtype)

		mask_regions = self.seconds2samples(mask_regions, fs)
		for region in mask_regions:
			if wavdata.ndim == 1:
				wavdata[region[0]:region[1]] = silenceValue

		wavfile.write(outwavfile, fs, wavdata)

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument('inwavfile', type=str, help='Path to input wav file')
	parser.add_argument('outwavfile', type=str, help='Path to output wav file')

	args = parser.parse_args()

	wrm = WavRegionMasker()
	regions = []
	regions.append((0, 0.5)) 
	regions.append((2, 2.5))
	wrm.mask_regions_in_wav(args.inwavfile, regions, args.outwavfile)
