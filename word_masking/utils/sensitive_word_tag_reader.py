#!/usr/bin/env python

# This file is a part of the word masking tool
# developed as part of the COMPRISE project
# Author(s): Imran Sheikh
# Copyright © 2019 INRIA
#
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

class SensitiveWordTagReader:
	def __init__(self, filepath, corpus='verbmobil', format='conll_ner', taglist=['PER', 'LOC', 'ORG', 'DATE', 'TIME']):
		self.filepath = filepath
		self.corpus = corpus
		self.format = format
		self.taglist = taglist
		self.utteranceCount = self.getUtteranceCount()
		self.filehandler = open(self.filepath, 'r')
		self.continueUtterance = 1

		if (self.corpus == 'verbmobil') and (self.format == 'conll_ner'):
			if self.taglist == 'ALL':
				self.read_utterance_words_and_tags = self.read_vm_utterance_words_and_all_conll_tags
			else:
				self.read_utterance_words_and_tags = self.read_vm_utterance_words_and_selected_conll_tags
		else:
			raise Exception('SensitiveWordTagReader: Unsupported corpus and tag format combination : ' + self.format + ' , ' + self.corpus)

	def getUtteranceCount(self):
		cnt = 0
		with open(self.filepath, 'r') as fp:
			for line in fp:
				if ('VM1' in line) or ('VM2' in line):
					cnt += 1
		return cnt

	def read_vm_utterance_words_and_all_conll_tags(self):
		self.continueUtterance = 1
		wordList = []
		wordid = 0
		while True:
			wordList = []
			line = self.filehandler.readline()
			if "" == line:
				self.filehandler.close()
				break
			elif ('VM1' in line) or ('VM2' in line):
				uttid, tag = line.rstrip().split()
				self.continueUtterance = 1
				wordid = 0

				while self.continueUtterance:
					line = self.filehandler.readline()
					if not line.strip():
						self.continueUtterance = 0
					else:
						word, tag = line.rstrip().split()
						w = {"wid": wordid, "word": word, "tag": tag}
						wordList.append(w)
						wordid+=1

			yield uttid, wordList

	def read_vm_utterance_words_and_selected_conll_tags(self):
		self.continueUtterance = 1
		wordList = []
		wordid = 0
		while True:
			wordList = []
			line = self.filehandler.readline()
			if "" == line:
				self.filehandler.close()
				break
			elif ('VM1' in line) or ('VM2' in line):
				uttid, tag = line.rstrip().split()
				self.continueUtterance = 1
				wordid = 0

				while self.continueUtterance:
					line = self.filehandler.readline()
					if not line.strip():
						self.continueUtterance = 0
					else:
						word, tag = line.rstrip().split()
						if '-' in tag:
							tag = tag.split('-')[1]
						if tag in self.taglist:
							w = {"wid": wordid, "word": word, "tag": tag}
							wordList.append(w)
						wordid+=1

			yield uttid, wordList

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument('input_path', type=str, help='Path to sensitive word tagged file')
	parser.add_argument('corpus', type=str, help='Corpus id e.g. verbmobil')
	parser.add_argument('tag_format', type=str, help='Tag format e.g. conll_ner')
	parser.add_argument('tag_csv', type=str, help='Target tags as csv e.g. PER,LOC,ORG')

	args = parser.parse_args()

	reader = SensitiveWordTagReader(args.input_path, args.corpus, args.tag_format, args.tag_csv.split(','))
	for uttid, wordList in reader.read_utterance_words_and_tags():
		print(uttid),
		for w in wordList:
			print(" " + str(w["wid"]) + "," + w["word"] + "," + w["tag"]),
		print ''

