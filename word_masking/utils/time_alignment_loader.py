#!/usr/bin/env python

# This file is a part of the word masking tool
# developed as part of the COMPRISE project
# Author(s): Imran Sheikh
# Copyright © 2019 INRIA
#
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

def load_ctm(ctmpath):
	wrdlist = []
	with open(ctmpath) as fp:
		for line in fp:
			if not(("[noise" in line) or ("<unk>" in line) or ("[hes]" in line) or ("<eps>" in line)):
				uid, ch, beg, dur, wrd =  line.rstrip().split() # ACKVMX_q003nxx0_001_130020_VM1_06 1 4.95 0.33 flight 
				w = {"word": wrd, "beg": beg, "dur": dur}
				wrdlist.append(w)
	return wrdlist

def time_alignment_loader(path, format='ctm'):
	if format == 'ctm':
		return load_ctm(path)
	else:
		raise Exception('Unsupported word-time alignment format: ' + format)

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument('input_path', type=str, help='Path to word-time alignment file')
	parser.add_argument('input_format', type=str, help='Format of word-time alignment file')

	args = parser.parse_args()

	print(time_alignment_loader(args.input_path, args.input_format))
