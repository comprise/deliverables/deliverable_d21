#!/usr/bin/env python

# This file is a part of the word masking tool
# developed as part of the COMPRISE project
# Author(s): Imran Sheikh
# Copyright © 2019 INRIA
#
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

import os.path

from tqdm import tqdm
from utils.sensitive_word_tag_reader import SensitiveWordTagReader

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser()

	parser.add_argument('word_tag_file', type=str, help='Path to sensitive word tagged file')
	parser.add_argument('tag_format', type=str, help='Tag format e.g. conll_ner')
	parser.add_argument('tag_csv', type=str, help='Target tags as csv e.g. PER,LOC,ORG')
	parser.add_argument('corpus', type=str, help='Corpus tag e.g. verbmobil')
	parser.add_argument('output_file', type=str, help='Output file')

	args = parser.parse_args()

	if not os.path.isfile(args.word_tag_file):
		raise FileNotFoundError(args.input_path)

	if args.tag_csv == 'ALL':
		taglist = 'ALL'
	else:
		taglist = args.tag_csv.split(',')

	transcripts = {}
	reader = SensitiveWordTagReader(args.word_tag_file, args.corpus, args.tag_format, 'ALL')
	for uttid, tagged_wordList in tqdm(reader.read_utterance_words_and_tags(), total=reader.utteranceCount):
		tr = ''
		for tagged_word in tagged_wordList:
			word = tagged_word["word"]
			tag = tagged_word["tag"]
			
			if '-' in tag:
				tag = tag.split('-')[1]
			
			if tag in taglist:
				tr = tr + '<masked> '
			else:
				tr = tr + word + ' '

		transcripts[uttid] = tr.rstrip()

	
	with open(args.output_file, 'w') as fp:
		for uttid in sorted(transcripts):
			fp.write(uttid + ' ' + transcripts[uttid] + '\n')
	fp.close()
