The word masking tool helps to:
* mask sensitive words in speech files
* mask sensitive words in speech transcription files

----

## Requirements
Python 2.7 or 3 with following packages:
* numpy
* scipy
* tqdm

## Usage
### Masking sensitive words in a speech file
Given a set of speech files, their corresponding word level transcriptions and the word level tags the following script helps to mask all the sensitive words in all the speech files:

> `python mask_words_in_speech.py word_tag_file tag_format tag_csv corpus corpus_path time_alignments output_path`
  
  `word_tag_file` contains a list of all words in all the expected utterances. (Currently supoorts only the [CoNLL NER format](https://www.aclweb.org/anthology/W03-0419) with only NER tags.)\
  `tag_format` is the format of the word_tag_file file. (Currently supports only **conll_ner**)\
  `tag_csv` is the csv of tags to be considered as sensitive words e.g. PER,LOC,ORG\
  `corpus` is the name of the speech corpus. (Currently only 'verbmobil' is supported.)\
  `corpus_path` is the path to the speech corpus\
  `time_alignments` is the directory containing the time alignments. (Currently only [CTM](http://www1.icsi.berkeley.edu/Speech/docs/sctk-1.2/infmts.htm#ctm_fmt_name_0) format is supported.)\
  `output_path` is the output directory where the masked speech files will be created\

The script will create a **wav** directory containing the masked speech files and a [Kaldi](https://kaldi-asr.org/doc/data_prep.html#data_prep_data_yourself) style *wav.scp* file.

### Masking sensitive words in a speech file
Given a file with word level tags the following script helps to mask all the sensitive words in all the speech transcription files:

> `mask_words_in_transcripts.py word_tag_file tag_format tag_csv corpus output_file`

  `word_tag_file` contains a list of all words in all the expected utterances. (Currently supoorts only the [CoNLL NER format](https://www.aclweb.org/anthology/W03-0419) with only NER tags.)\
  `tag_format` is the format of the word_tag_file file. (Currently supports only **conll_ner**)\
  `tag_csv` is the csv of tags to be considered as sensitive words e.g. PER,LOC,ORG\
  `corpus` is the name of the speech corpus. (Currently only 'verbmobil' is supported.)\
  `output_file` is output word sensitive words masked transcription file\

The output file is a [Kaldi](https://kaldi-asr.org/doc/data_prep.html#data_prep_data_yourself) style *text* transcription file.
