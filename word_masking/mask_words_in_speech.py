#!/usr/bin/env python
# -*- encoding: utf-8 -*-


# This file is a part of the word masking tool
# developed as part of the COMPRISE project
# Author(s): Imran Sheikh
# Copyright © 2019 INRIA
#
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

import os.path

from tqdm import tqdm

from utils.sensitive_word_tag_reader import SensitiveWordTagReader
from utils.time_alignment_loader import time_alignment_loader
from utils.helpers import verbmobil_eng_id2wav, verbmobil_eng_id2ctm
from utils.mask_regions_in_wav import WavRegionMasker

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser()

	parser.add_argument('word_tag_file', type=str, help='Path to sensitive word tagged file')
	parser.add_argument('tag_format', type=str, help='Tag format e.g. conll_ner')
	parser.add_argument('tag_csv', type=str, help='Target tags as csv e.g. PER,LOC,ORG')

	parser.add_argument('corpus', type=str, help='Corpus tag e.g. verbmobil')
	parser.add_argument('corpus_path', type=str, help='Path to corpus directory')

	parser.add_argument('time_alignments', type=str, help='Path to input time alignment files')

	parser.add_argument('output_path', type=str, help='Path to output directory')

	args = parser.parse_args()

	if not os.path.isfile(args.word_tag_file):
		raise FileNotFoundError(args.input_path)

	if not os.path.isdir(args.corpus_path):
		raise FileNotFoundError(args.corpus_path)

	if not os.path.isdir(args.time_alignments):
		raise FileNotFoundError(args.time_alignments)

	taglist = args.tag_csv.split(',')

	out_wav_dir = ''
	if not os.path.isdir(args.output_path):
		raise FileNotFoundError(args.output_path)
	else:
		out_wav_dir = os.path.join(args.output_path, 'wav')
		out_wav_dir = os.path.abspath(out_wav_dir)
		if not os.path.isdir(out_wav_dir):
			os.makedirs(out_wav_dir)
		else:
			print('Info: Will be overwriting contents of '+out_wav_dir)

	maskcnt = 0
	wav_paths = {}

	reader = SensitiveWordTagReader(args.word_tag_file, args.corpus, args.tag_format, taglist)
	for uttid, tagged_wordList in tqdm(reader.read_utterance_words_and_tags(), total=reader.utteranceCount):
		if len(tagged_wordList)>0:
			inwav = verbmobil_eng_id2wav(uttid, cd_suffix='.1', corpus_path=args.corpus_path)
			inctm = verbmobil_eng_id2ctm(uttid, args.time_alignments)
			outwav = os.path.join(out_wav_dir, os.path.basename(inwav))

			if inctm is None:
				print('\nWarning: Verbmobil ctm file does not exist for: ' + uttid)
				print('Warning: This is most likely due to alignment ground truth alignment problems. A masked version of this file will not be created!')
				wav_paths[uttid] = verbmobil_eng_id2wav(uttid)
			else:
				alignments = time_alignment_loader(inctm)
				
				mask_regions = []
				for tagged_word in tagged_wordList:
					ctm_word = alignments[tagged_word["wid"]]
					if ctm_word["word"] != tagged_word["word"]:
						print('Error: Mismatch in word-time alignment and senitive word tag output for ' + uttid)
						print('      '+ inwav + ' will not be masked!')
					else:
						region_start = float(ctm_word["beg"])
						region_end = float(ctm_word["beg"]) + float(ctm_word["dur"])
						region = (region_start, region_end)
						mask_regions.append(region)

				wrm = WavRegionMasker()
				wrm.mask_regions_in_wav(inwav, mask_regions, outwav)

				wav_paths[uttid] = outwav
				maskcnt+=1
		else:
			wav_paths[uttid] = verbmobil_eng_id2wav(uttid)

	wav_scp_file = os.path.join(args.output_path, 'wav.scp')
	with open(wav_scp_file, 'w') as fp:
		for uttid in sorted(wav_paths):
			fp.write(uttid + ' ' + wav_paths[uttid] + '\n')
	fp.close()


	print('Masked words in ' + str(maskcnt) + ' speech files out of ' + str(reader.utteranceCount))

