export KALDI_ROOT="/talc3/multispeech/calcul/users/isheikh/tools/kaldi-a4b6388/"
export PATH="/talc3/multispeech/calcul/users/isheikh/tools/flac/flac-1.3.2/bin/":$PATH

export WD=`readlink -f .`
export PATH=$WD:$WD/utils:$KALDI_ROOT/src/bin:$KALDI_ROOT/tools/openfst/bin:$KALDI_ROOT/tools/kaldi_lm/:$PATH
export LD_LIBRARY_PATH=$KALDI_ROOT/tools/openfst/lib:$LD_LIBRARY_PATH
export LC_ALL=C

[ ! -f $KALDI_ROOT/tools/config/common_path.sh ] && echo >&2 "The standard file $KALDI_ROOT/tools/config/common_path.sh is not present -> Exit!" && exit 1
. $KALDI_ROOT/tools/config/common_path.sh
