This code does forced alignment of a set of speech files and their corresponding text transcriptions. It generates two type of alignments: (1) alignment at the word level, and (b) alignment at the phone level.

Please go through [prerequisites and assumptions](#prerequisites-and-assumptions) before going on to the [setup](#setup) and [usage](#usage) of this code .

## Prerequisites and Assumptions
- The code re-uses binaries and scripts from the [Kaldi](http://kaldi-asr.org) toolkit. So you should have Kaldi pre-installed on your system. 
- The code requires that you have a pre-trained Kaldi GMM acoustic model and a corresponding `lang/` directory. The Librispeech speech model shared along with this code can be used for your purpose, provided that you are dealing with clean 16kHz speech data. Otherwise you can choose a suitable pre-trained Kaldi GMM model from the [Kaldi branches]( http://kaldi-asr.org/downloads/all/)

## Setup
- Ensure that you have a working Kaldi installation.
- Modify the softlinks `steps/` and `utils/` in this directory to point to `egs/wsj/s5/steps/` and `egs/wsj/s5/utils`, respectively, in your Kaldi installation.
- Modify the path of KALDI_ROOT and modify/remove the path to flac utility in [path.sh](path.sh)
- Modify [cmd.sh](cmd.sh) if you are using a different execution queue for Kaldi.
- Modify files in [conf/](conf/) as per your pre-trained Kaldi GMM acoustic model.

## Usage
- Ensure that you have completed the steps in the setup section above.
- Similar to Kaldi acoustic model training, prepare a data/ directory containing the following files:

        'text' file containing audio file id and its corresponding cleaned text transcription, one per line.\
        'wav.scp' file listing the audio file ids and the corresponding audio file access descriptor.\
        'utt2spk' file mapping utterance ids to speaker ids\
        'spk2utt' file mapping speaker ids to utterance ids
  
  For examples of these files refer [Kaldi data preparation](http://kaldi-asr.org/doc/data_prep.html) or look into `task_libri/data/test_clean/` directory shared separately with this code.

- Verify the number of jobs specified in the beginning of align.sh script
- Launch the alignment script as follows:

    >`bash align.sh <data_dir> <mdl_dir> <lang_dir> <ali_dir>"`

        data_dir directory containing text wav.scp utt2spk spk2utt files\
        mdl_dir directory with the acoustic model for alignment\
        lang_dir directory for alignment\
        ali_dir is output directory for output alignments

    For example, to try out using the Librispeech model and data shared along with this code:

    >`bash align.sh task_libri/data/test_clean/ task_libri/model/tri6b_vassil/ task_libri/model/lang_vassil/ task_libri/ali/test_clean/ 2>&1 | tee task_libri/align_test-clean.log`

    After successful completion, this script should create phone level and word level alignments in [CTM format](http://www1.icsi.berkeley.edu/Speech/docs/sctk-1.2/infmts.htm#ctm_fmt_name_0) in `<ali_dir>/phn_ali/` and `<ali_dir>/wrd_ali/` directories, respectively.

## Note
- The current version does not explicitly handle non-verbal lexical entities and non-silence phones like [noise], [laughter], etc. 
- The number of jobs used by the alignment process is hardcoded to 16 in [align.sh](align.sh).
- `task_libri` sample data is available at `/talc3/multispeech/calcul/users/isheikh/exp/align-s2t/task_libri` on Nancy site of Grid5000.


