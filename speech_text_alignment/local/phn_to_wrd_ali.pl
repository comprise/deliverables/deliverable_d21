#!/bin/bash

# Copyright © 2019 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if($#ARGV != 2){
	print STDERR "USAGE: $0 <phn_ali_dir> <align_lexicon.txt> <wrd_ali_dir>";
	exit(0);
}

my %lexLookup;
open my $FH, $ARGV[1] or die "Could not open $ARGV[1]: $!";
while( my $line = <$FH>)  {   
	$line =~ s/\r*\n+//;
    my ($w, $ww, $phnseq) = split(/\s+/, $line, 3);
    if(exists($lexLookup{$phnseq})){
    	$lexLookup{$phnseq} .= ",$w";
    }else{
    	$lexLookup{$phnseq} = $w;
    }
    #print $w, " ", $phnseq, "\n";
}
close $FH;

my $lastid = "";
my $wrdctm = "";
my $wbeg = 0;
my $wdur = 0;
my $wphnseq = "";
foreach my $phnctm (glob("$ARGV[0]/*.ctm")) {
	print "Extracting word alignments from $phnctm\n";
	my $fopenflag = 0;
	open my $FH, "<", $phnctm or die "can't read open '$phnctm': $OS_ERROR";
	while (my $line = <$FH>) {
		$line =~ s/\r*\n+//;
		my ($fileid, $ch, $beg, $dur, $phn) = split(/\s+/, $line);

		if(($fileid ne $lastid) && $fopenflag){
			my $wctmfile = $ARGV[2]."/".$lastid.".ctm";
			open(my $FOUT, '>', $wctmfile) or die "can't read open '$wctmfile': $OS_ERROR";
			print $FOUT $wrdctm;
			close $FOUT;
			$wrdctm = "";
		}

		if($phn =~ m/^SIL$/i){	# note that non_silence phones are not yet handled!
			$wrdctm = $wrdctm.$line."\n";
		}else{
			my ($p, $type) = split(/\_/, $phn);
			if($type eq 'B'){
				$wbeg = $beg;
				$wphnseq = $phn;
				$wdur = $dur;
			}elsif($type eq 'E'){
				$wdur += $dur;
				$wphnseq .= " $phn";
				$wrdctm .= "$fileid $ch $wbeg $wdur $lexLookup{$wphnseq}\n";
				$wbeg = 0;
				$wdur = 0;
				$wphnseq = "";
			}elsif($type eq 'I'){
				$wdur += $dur;
				$wphnseq .= " $phn";
			}elsif($type eq 'S'){
				$wrdctm = $wrdctm."$fileid $ch $beg $dur $lexLookup{$phn}\n";
			}else{
				print STDERR "Phone type error: Did not expect $phn\n";
			}
		}

		$fopenflag = 1;
		$lastid = $fileid;
	}
	close $FH or die "can't read close '$fp': $OS_ERROR";

	my $wctmfile = $ARGV[2]."/".$lastid.".ctm";
	open(my $FOUT, '>', $wctmfile) or die "can't read open '$wctmfile': $OS_ERROR";
	print $FOUT $wrdctm;
	close $FOUT;
	$wrdctm = "";
}
