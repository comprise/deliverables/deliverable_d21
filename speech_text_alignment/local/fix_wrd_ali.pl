#!/bin/bash

# Copyright © 2019 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)


if($#ARGV != 2){
	print STDERR "USAGE: $0 <wrd_ali_tmp_dir> <text> <wrd_ali_dir>\n";
	exit(0);
}

open my $FH, $ARGV[1] or die "Could not open $ARGV[1]: $!";
while( my $line = <$FH>)  {   
	$line =~ s/\r*\n+//;
    my ($id, $txt) = split(/\s+/, $line, 2);
    my @txtwrds = split(/\s+/, $txt);
    my $wid = 0;
    my $inctm = $ARGV[0]."/$id.ctm";
    if(not -e $inctm){
    	print STDERR "Could not perform forced alignment for $id\n";
    	next;
    }
    my $outctm = $ARGV[2]."/$id.ctm";
    open my $FH2, $inctm or die "Could not open $inctm : $!";
    open my $FH3, '>', $outctm or die "Could not open $outctm : $!";
	while(my $ctmline = <$FH2>){
		$ctmline =~ s/\r*\n+//;
		my ($fileid, $ch, $beg, $dur, $ctmwrd) = split(/\s+/, $ctmline);
		if($ctmwrd ne 'SIL'){	# note that non_silence lexical units are not yet handled!
			my @tmp = split(/\,/, $ctmwrd);
			foreach my $w (@tmp){
				if($w eq $txtwrds[$wid]){
					print $FH3 "$fileid $ch $beg $dur $w\n";
					$wid += 1;
					last;
				}
			}	
		}else{
			print $FH3 "$fileid $ch $beg $dur $ctmwrd\n";
		}
	}
	close $FH2;
	close $FH3;
}
close $FH;

