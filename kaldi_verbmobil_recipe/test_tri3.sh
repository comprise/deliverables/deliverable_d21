#!/bin/bash

# Derived software, Copyright © 2019 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
#
# Based on Kaldi, Copyright © 2019 Johns Hopkins University
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
# https://github.com/kaldi-asr/kaldi/blob/master/COPYING

feats_nj=20	     # num of jobs for feat extraction
decode_nj=20	     # num of jobs for decode

fe_stage=false

wav_dir=/talc/multispeech/corpus/dialog/Verbmobil/Verbmobil/  # absolute path to dir containing Verbmobil signal files
testSplit=test

. ./path.sh  # set paths to binaries
. ./cmd.sh   ## This relates to the execution queue.

set -e

. utils/parse_options.sh  # e.g. this parses the options if supplied. (currently disabled.)

if $fe_stage; then
  echo ============================================================================
  echo "         MFCC Feature Extration & CMVN for Test set          "
  echo ============================================================================
  for x in $testSplit; do
    mkdir -p data/split/$x
    perl local/generateWavScp.pl $wav_dir data/orig/transcriptions data/split/$x/ || exit 1;
    sort data/split/$x/wav.scp.unsorted > data/split/$x/wav.scp
    sort data/split/$x/utt2spk.unsorted > data/split/$x/utt2spk
    sort data/split/$x/spk2utt.unsorted > data/split/$x/spk2utt
    sort data/split/$x/text.unsorted > data/split/$x/text
    rm -f data/split/$x/*.unsorted

    steps/make_mfcc.sh --cmd "$train_cmd" --nj $feats_nj data/split/$x || exit 1;
    steps/compute_cmvn_stats.sh data/split/$x || exit 1;
  done
fi

steps/decode_fmllr.sh --nj "$decode_nj" --cmd "$decode_cmd" exp/tri3_full/graph data/split/$testSplit exp/tri3_full/decode_test
