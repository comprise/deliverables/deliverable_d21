#!/usr/bin/perl

# Copyright © 2019 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if($#ARGV != 2){
	print STDERR "USAGE: $0 <signal_parent_directory> <orig_text_trascriptions> <out_dir>\n";
	exit(0);
}

use File::Spec;
use File::Basename;

my @vmcds = qw/6 8 13 23 28 31 32 42 43 47 50 51 52 55 56/;	# Verbmobil English CDs
my $probSpkrsA = " CAC JDH NKH RGM ";	# speaker ids representing different speakers in VM1 and VM2; keep the begin n end spaces
my $probSpkrsB = "MAS";					# speaker id  representing different speakers in VM1

my %signalPath;
my @dirs = (File::Spec->rel2abs($ARGV[0]));
while (@dirs) {
	my $thisdir = shift @dirs;
	opendir my $dh, $thisdir;
	while (my $entry = readdir $dh) {
		next if $entry eq '.';
		next if $entry eq '..';
		
		my $fullname = "$thisdir/$entry";
				
		if (-d $fullname ){
			push @dirs, $fullname;
		}elsif($fullname =~ m/\.wav$/){		
			for my $cd (@vmcds){
				if($fullname =~ m/VM$cd\.1\/data\/.+\.wav$/){
					my $fname = basename($fullname);
					$fname =~ s/\.wav$//;
					$fname =~ s/\_ENG$//;
					$signalPath{$fname} = $fullname;
				}
			}
		}
	}
}

my $outdir = $ARGV[2];
$outdir =~ s/\/$//;
my %spk2utt;
my $txtfile = "$outdir/text.unsorted";
my $wavscpfile = "$outdir/wav.scp.unsorted";
my $utt2spkfile = "$outdir/utt2spk.unsorted";
open(TEXTF, '>', $txtfile) or die $!;
open(WAVSCP, '>', $wavscpfile) or die $!;
open(UTT2SPK, '>', $utt2spkfile) or die $!;
open my $FH, "<", $ARGV[1] or die "can't read open '$ARGV[1]': $OS_ERROR";
while (my $line = <$FH>) {
	my ($id, $txt) = split(/\s+/, $line, 2);
	my ($vmid, $dialogid, $turnid, $spkrid, $cdversion, $cdid) = split(/\_/, $id);
	my $sigid = $dialogid."_".$turnid."_".$spkrid;
	if($probSpkrsA =~ m/ $spkrid /){
		$spkrid = $spkrid.$vmid; # append VM id coz speaker id represents different speakers in VM1 and VM2		
	}elsif(($spkrid eq $probSpkrsB) && ($dialogid =~ m/q007n/i)){
		$spkrid = $spkrid."VMZ"; # to keep speaker ids consistent
	}else{
		$spkrid = $spkrid."VMX"; # to keep speaker ids consistent
	}
	$cdid = sprintf("%02d", $cdid);
	my $newid = $spkrid."_".$dialogid."_".$turnid."_".$cdversion."_".$vmid."_".$cdid;		
	if(exists $signalPath{$sigid}){
		print TEXTF $newid," ",$txt;
		print WAVSCP $newid," ",$signalPath{$sigid},"\n";
		print UTT2SPK $newid," ",$spkrid,"\n";
		if(exists $spk2utt{$spkrid}){
			$spk2utt{$spkrid} = "$spk2utt{$spkrid} $newid";
		}else{
			$spk2utt{$spkrid} = "$newid";
		}
	}else{
		print "Warning: Missing wav file corresponding to $id\n";
	}
}
close(TEXTF);
close(UTT2SPK);
close(WAVSCP);
close($FH);

my $spk2uttfile = "$outdir/spk2utt.unsorted";
open(SPK2UTT, '>', $spk2uttfile) or die $!;
foreach my $spkr (keys %spk2utt){
	my $list = $spk2utt{$spkr};
	$list =~ s/^\s+//;
	my @tmp = split(/\s+/, $list);
	my @sorted = sort @tmp;
	my $utts = join(" ", @sorted);
	print SPK2UTT "$spkr $utts\n";
}
close(SPK2UTT);

exit(0);


