#!/bin/bash

# Derived software, Copyright © 2019 INRIA (Imran Sheikh) 
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
#
#
# Based on Kaldi (kaldi/egs/wsj/s5/local/chain/tuning/run_tdnn_1b.sh), Copyright 2019 © Johns Hopkins University (author: Daniel Povey)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
# https://github.com/kaldi-asr/kaldi/blob/master/COPYING


echo "$0 $@"  # Print the command line for logging

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

test_sets=split/train
stage=0
nj=40

nnet3_affix=         # affix for exp dirs, e.g. it was _cleaned in tedlium.
affix=1f             #affix for TDNN+LSTM directory e.g. "1a" or "1b", in case we change the configuration.
tree_dir=exp/chain${nnet3_affix}/tree_a_sp
dir=exp/chain${nnet3_affix}/tdnn${affix}_sp

# training chunk-options
chunk_width=140,100,160
# we don't need extra left/right context for TDNN systems.
chunk_left_context=0
chunk_right_context=0

if [ $stage -le 0 ]; then
	for datadir in ${test_sets}; do
		utils/copy_data_dir.sh data/$datadir data/${datadir}_hires

		# extract mfcc, compute cmvn
		steps/make_mfcc.sh --nj $nj --mfcc-config conf/mfcc_hires.conf \
	    	--cmd "$train_cmd" data/${datadir}_hires
	    steps/compute_cmvn_stats.sh data/${datadir}_hires
	    utils/fix_data_dir.sh data/${datadir}_hires

	    # extract iVectors for the test data
    	nspk=$(wc -l <data/${datadir}_hires/spk2utt)
    	steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj "${nspk}" \
      		data/${datadir}_hires exp/nnet3${nnet3_affix}/extractor \
      		exp/nnet3${nnet3_affix}/ivectors_${datadir}_hires

	done
fi

if [ $stage -le 1 ]; then
	frames_per_chunk=$(echo $chunk_width | cut -d, -f1)
	for datadir in ${test_sets}; do
		data_affix=$(echo $datadir | sed "s/\//_/")
    	nspk=$nj #$(wc -l <data/${datadir}_hires/spk2utt) # $nj gives consistent output
      	for lmtype in tg; do
	        steps/nnet3/decode.sh \
	          --acwt 1.0 --post-decode-acwt 10.0 \
	          --extra-left-context $chunk_left_context \
	          --extra-right-context $chunk_right_context \
	          --extra-left-context-initial 0 \
	          --extra-right-context-final 0 \
	          --frames-per-chunk $frames_per_chunk \
	          --nj $nspk --cmd "$decode_cmd"  --num-threads 4 \
	          --online-ivector-dir exp/nnet3${nnet3_affix}/ivectors_${datadir}_hires \
	          $tree_dir/graph_${lmtype} data/${datadir}_hires ${dir}/decode_${lmtype}_${data_affix} || exit 1
	    done
	done
fi
