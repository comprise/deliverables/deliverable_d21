#!/bin/bash

# Derived software, Copyright © 2019 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
#
# Based on Kaldi, Copyright © 2019 Johns Hopkins University
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
# https://github.com/kaldi-asr/kaldi/blob/master/COPYING

lmwt=10				# chain 10		tri3, nnet3 12
wip=0.0				# chain 0.0		tri3, nnet3 1.0
frame_shift=0.03	# chain 0.03	tri3, nnet3 0.01

. ./path.sh  # set paths to binaries
. ./cmd.sh   ## This relates to the execution queue.

set -e

. utils/parse_options.sh  # e.g. this parses the options if supplied. (currently disabled.)

if [ $# != 4 ]; then
    echo "USAGE: $0 <data_dir> <lang_dir> <decode_dir> <ctm_out_dir>"
    echo "          data_dir directory containing text wav.scp utt2spk spk2utt files."
    echo "          lang_dir directory for alignment"
    echo "          decode_dir directory with decoded outputs"
    echo "          ctm_out_dir directory for output ctm files"
    exit
fi

echo "$0 $@"  # Print the command line for logging

mkdir $4/wrd_ali_tmp
steps/get_ctm_fast.sh --lmwt $lmwt --wip $wip --frame_shift $frame_shift --print_silence true $1 $2 $3 $4/wrd_ali_tmp

mkdir $4/wrd_ali
perl local/split_ctms.pl $4/wrd_ali_tmp/ctm $4/wrd_ali