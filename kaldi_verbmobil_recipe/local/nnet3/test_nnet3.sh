#!/bin/bash

# Derived software, Copyright © 2019 INRIA (Imran Sheikh) 
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
#
#
# Based on Kaldi (kaldi/egs/wsj/s5/local/nnet3/tuning/run_tdnn_1a.sh), Copyright 2019 © Johns Hopkins University (author: Daniel Povey, Ewald Enzinger)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
# https://github.com/kaldi-asr/kaldi/blob/master/COPYING

echo "$0 $@"  # Print the command line for logging

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

test_sets=split/test
stage=1
nj=30
gmm=tri3_full    # this is the source gmm-dir that we'll use for alignments; it
                 # should have alignments for the specified training data.
nnet3_affix=       # affix for exp dirs, e.g. it was _cleaned in tedlium.
tdnn_affix=1a      #affix for TDNN directory e.g. "1a" or "1b", in case we change the configuration.affix=1f             #affix for TDNN+LSTM directory e.g. "1a" or "1b", in case we change the configuration.
dir=exp/nnet3${nnet3_affix}/tdnn${tdnn_affix}_sp
gmm_dir=exp/${gmm}

if [ $stage -le 0 ]; then
	for datadir in ${test_sets}; do
		utils/copy_data_dir.sh data/$datadir data/${datadir}_hires

		# extract mfcc, compute cmvn
		steps/make_mfcc.sh --nj $nj --mfcc-config conf/mfcc_hires.conf \
	    	--cmd "$train_cmd" data/${datadir}_hires
	    steps/compute_cmvn_stats.sh data/${datadir}_hires
	    utils/fix_data_dir.sh data/${datadir}_hires

	    # extract iVectors for the test data
    	nspk=$(wc -l <data/${datadir}_hires/spk2utt)
    	steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj "${nspk}" \
      		data/${datadir}_hires exp/nnet3${nnet3_affix}/extractor \
      		exp/nnet3${nnet3_affix}/ivectors_${datadir}_hires
	done
fi

if [ $stage -le 1 ]; then
  for data in $test_sets; do
    (
      data_affix=$(echo $data | sed "s/\//_/")
      nj=$(wc -l <data/${data}_hires/spk2utt)
        graph_dir=$gmm_dir/graph
        steps/nnet3/decode.sh --nj $nj --cmd "$decode_cmd"  --num-threads 4 \
           --online-ivector-dir exp/nnet3${nnet3_affix}/ivectors_${data}_hires \
          ${graph_dir} data/${data}_hires ${dir}/decode_${data_affix} || exit 1
    ) || touch $dir/.error &
  done
fi
