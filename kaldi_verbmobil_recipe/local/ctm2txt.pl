#!/usr/bin/perl

# Copyright © 2019 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if($#ARGV != 0){
	print STDERR "USAGE: $0 <ctm_directory>\n";
	exit(0);
}

use File::Basename;

my @dirs = ($ARGV[0]); 
while (@dirs) {
	my $thisdir = shift @dirs;
	opendir my $dh, $thisdir;
	while (my $entry = readdir $dh) {
		next if $entry eq '.';
		next if $entry eq '..';
		
		my $fullname = "$thisdir/$entry";
				
		if (-d $fullname) {
			push @dirs, $fullname;
		}elsif($fullname =~ m/\.ctm$/){
			my $uttid = basename($fullname);
			$uttid =~ s/\.ctm$//;
			my $out = "$uttid";
			open my $FH, "<", $fullname or die "can't read open '$fullname': $OS_ERROR";
			while (my $line = <$FH>) {
				$line =~ s/\s+$//;
				my @fields = split(/\s/, $line);	# ADBVMX_e041ach1_001_280000_VM2_28 1 0.040 0.150 okay 
				if($fields[4] !~ m/\<eps\>/){
					$out = $out." ".$fields[4];
				}
			}
			close($FH);
			print $out."\n";
		}
	}
}