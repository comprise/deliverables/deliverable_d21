export KALDI_ROOT="/talc3/multispeech/calcul/users/isheikh/tools/kaldi-a4b6388/"
export KALDI_LM_ROOT="/talc3/multispeech/calcul/users/isheikh/tools/kaldi_lm"
export WD=`readlink -f .`
export PATH=$WD:$WD/utils:$KALDI_ROOT/src/bin:$KALDI_ROOT/tools/openfst/bin:$KALDI_LM_ROOT/:$PATH
export LD_LIBRARY_PATH=$KALDI_ROOT/tools/openfst/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}

export LC_ALL=C

export SOX_ROOT="/talc3/multispeech/calcul/users/isheikh/tools/sox-14.4.2/src/"
export PATH=$SOX_ROOT:$PATH
export LD_LIBRARY_PATH=$SOX_ROOT/.libs:$LD_LIBRARY_PATH

[ ! -f $KALDI_ROOT/tools/config/common_path.sh ] && echo >&2 "The standard file $KALDI_ROOT/tools/config/common_path.sh is not present -> Exit!" && exit 1
. $KALDI_ROOT/tools/config/common_path.sh
