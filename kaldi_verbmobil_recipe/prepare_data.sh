#!/bin/bash

# Copyright © 2019 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if [ $# != 1 ]; then
    echo "USAGE: $0 <trl_dir>"
    echo "          <trl_dir> is the parent directory containing all .trl transcriptions (for all dialogs in all Verbmobil CDs)."
    exit
fi

trldir=$1

### some file checks
echo "Performing checks on $trldir ..."
trlcnt=`find $trldir -type f -name "*.trl" | wc -l`
if [ $trlcnt -lt 4000 ]; then	# 4000 chosen arbitarily, just to ensure that we have the right input directory
    printf "\nError: $trldir directory contains only $trlcnt .trl transcriptions.\n Please ensure it has enough transcriptions.\n\n"
    exit 0
fi

if [ -d data/local ]; then
	printf "\nWarning: data/local directory already exists. Backing it up to data.bckp/ and creating a new data/ directory!\n\n"
	cp -R data/ data.bckp
	mkdir data/local/dict
fi

### Predefined directories
transdir="data/trans/"				# directory for storing cleaned transcriptions
dictdir="data/local/dict/"			# directory to store all intial lexicon related files
initLMdir="data/local/local_lm"		# directory to store text for intial ARPA LM 

### Parse .trl transcriptions into a usable transcription file 
echo "Preparing transcriptions ..."
mkdir -p $transdir
perl local/getCleanTranscripts.pl $trldir > $transdir/transcriptions.orig

# get cmudict-0.7b
echo "Preparing dictionary ..."
wget https://raw.githubusercontent.com/Alexir/CMUdict/master/cmudict-0.7b -P $dictdir
if [[ "$?" != 0 ]]; then
    echo "\nError downloading https://raw.githubusercontent.com/Alexir/CMUdict/master/cmudict-0.7b. Exitting!\n\n"
else
    echo "Successfully downloaded https://raw.githubusercontent.com/Alexir/CMUdict/master/cmudict-0.7b !"
fi

### Mark OOVs based on word frequency count. Check local/markFreqBasedOOVs.pl for word frequency thresholds.
echo "Marking OOVs ..."
perl local/markFreqBasedOOVs.pl $transdir/transcriptions.orig $dictdir/cmudict-0.7b > $transdir/transcriptions.oov-marked
  
### Prepare the lexicon
echo "Preparing final lexicon ..."
perl local/lookupLexicon.pl $transdir/transcriptions.oov-marked $dictdir/cmudict-0.7b | sort -u > $dictdir/lexicon.txt.orig

### Prepare the data/local/dict/ directory with silence_phones.txt, 
echo "Preparing data/local/dict contents ..."
cat $dictdir/lexicon.txt.orig | cut -d " " -f2- | tr " " "\n" | sort -u | egrep "^[a-z][a-z]?$" > $dictdir/nonsilence_phones.txt
echo "sil" > $dictdir/optional_silence.txt
echo "sil" > $dictdir/silence_phones.txt
cat $dictdir/lexicon.txt.orig | cut -d " " -f2- | tr " " "\n" | sort -u | egrep "^[a-z][a-z][a-z]+" >> $dictdir/silence_phones.txt
cat $dictdir/silence_phones.txt | tr "\n" " " | sed 's/ *$/|/' | tr '|' '\n' > $dictdir/extra_questions.txt

### Prepare train-dev-test splits
echo "Preparing strain-dev-test splits ..."
perl local/getTrainDevTestSplits.pl $transdir/transcriptions.oov-marked $transdir/

### Prepepare intial LM text
mkdir -p $initLMdir
cut -d " " -f2- $transdir/train.transcriptions > $initLMdir/transcriptions.txt


