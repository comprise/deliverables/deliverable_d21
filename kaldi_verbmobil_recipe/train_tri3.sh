#!/bin/bash

# Kaldi recipe for traning GMM-HMM models on the English subset of the Verbmobil corpus.
#
# Derived software, Copyright © 2019 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
#
# Based on Kaldi, Copyright © 2019 Johns Hopkins University
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
# https://github.com/kaldi-asr/kaldi/blob/master/COPYING

lang_stage=true     # set to false if lang preparation step is already completed.
fe_stage=true       # set to false if mfcc feature extraction step is already completed.
split_stage=true    # set to false if mono/tri1/tri2 training spliting step is already completed.
mono_stage=true     # set to false if mono training step is already completed.
tri1_stage=true     # set to false if tri1 training step is already completed.
tri2_stage=true     # set to false if tri2 training step is already completed.
tri3_stage=true      # tri3 on partial trainset; set to false if this training step is already completed.
tri3_full_stage=true # tri3 on full trainset; set to false if this training step is already completed.

feats_nj=40	     # num of jobs for feat extraction
train_nj=40	     # num of jobs for train
decode_nj=20	     # num of jobs for decode

# Acoustic model parameters (tested for 40hrs dataset)
numLeavesTri1=2000
numGaussTri1=10000
numLeavesTri2=2500
numGaussTri2=15000
numLeavesTri3=2500
numGaussTri3=15000
numLeavesTri3Full=3500
numGaussTri3Full=25000

# number of utterances for different AM
monoSize=5000   # shortest
tri1Size=5000   # random
tri2Size=10000   # random

wav_dir=/talc/multispeech/corpus/dialog/Verbmobil/Verbmobil/	# absolute path to dir containing Verbmobil signal files
trainSplit=train    # directory for storing/using the train split data files
devSplit=dev        # directory for storing/using the dev   split data files
testSplit=test      # directory for storing/using the test  split data files

. ./path.sh  # set paths to binaries
. ./cmd.sh   # This relates to the execution queue.

set -e

. utils/parse_options.sh  # e.g. this parses the options if supplied. (currently disabled.)

if $lang_stage; then
  echo ============================================================================
  echo "                Data & Lexicon & Language Preparation                     "
  echo ============================================================================
  ### following steps will require contents in 'data_dir/local/dict' and 'data/local/local_lm'
  utils/prepare_lang.sh data/local/dict "<unk>" data/local/lang_tmp data/lang || exit 1;
  local/train_lm.sh || exit 1;        # might repeat some steps from the scripts run offline but it helps to keep in sync with Kaldi steps
  local/format_local_lm.sh || exit 1;
fi

if $fe_stage; then
  echo ============================================================================
  echo "         MFCC Feature Extration & CMVN for Training and Dev set          "
  echo ============================================================================
  for x in $trainSplit $devSplit $testSplit; do
    mkdir -p data/split/$x
    perl local/generateWavScp.pl $wav_dir data/trans/$x.transcriptions data/split/$x/ || exit 1;
    sort data/split/$x/wav.scp.unsorted > data/split/$x/wav.scp
    sort data/split/$x/utt2spk.unsorted > data/split/$x/utt2spk
    sort data/split/$x/spk2utt.unsorted > data/split/$x/spk2utt
    sort data/split/$x/text.unsorted > data/split/$x/text
    rm -f data/split/$x/*.unsorted

    steps/make_mfcc.sh --cmd "$train_cmd" --nj $feats_nj data/split/$x || exit 1;
    steps/compute_cmvn_stats.sh data/split/$x || exit 1;
  done
fi

if $split_stage; then
  echo ============================================================================
  echo "                Create Mono/Tri1/Tri2 splits for Training                 "
  echo ============================================================================
  ## make subset with shortest $monoSize utt (?hr data) to train mono models
  utils/subset_data_dir.sh --shortest data/split/$trainSplit $monoSize data/split/$trainSplit"_s"$monoSize || exit 1  # use 5k in a normal situation

  ## make subset with random $tri1Size utterances to train tri1 models.
  utils/subset_data_dir.sh data/split/$trainSplit $tri1Size data/split/$trainSplit"_r"$tri1Size || exit 1;  # use 5k in a normal situation

  ## make subset with random $tri2Size utterances to train tri2 models.
  utils/subset_data_dir.sh data/split/$trainSplit $tri2Size data/split/$trainSplit"_r"$tri2Size || exit 1;  # use 10k in a normal situation
fi

if $mono_stage; then
  echo ============================================================================
  echo "                     MonoPhone Training & Decoding                        "
  echo ============================================================================
  steps/train_mono.sh --nj "$train_nj" --cmd "$train_cmd" data/split/$trainSplit"_s"$monoSize data/lang exp/mono

  utils/mkgraph.sh data/lang_test_tg exp/mono exp/mono/graph

  steps/decode.sh --nj "$decode_nj" --cmd "$decode_cmd" exp/mono/graph data/split/$devSplit exp/mono/decode_dev 
fi

if $tri1_stage; then
  echo ============================================================================
  echo "           tri1 : Deltas + Delta-Deltas Training & Decoding               "
  echo ============================================================================
  # Align te-in-Train_r5k data with mono models.
  steps/align_si.sh --boost-silence 1.25 --nj "$train_nj" --cmd "$train_cmd" data/split/$trainSplit"_r"$tri1Size data/lang exp/mono exp/mono_ali

  # From monophone model, train tri1 which is Deltas + Delta-Deltas.
  steps/train_deltas.sh --cmd "$train_cmd" $numLeavesTri1 $numGaussTri1 data/split/$trainSplit"_r"$tri1Size data/lang exp/mono_ali exp/tri1

  utils/mkgraph.sh data/lang_test_tg exp/tri1 exp/tri1/graph

  steps/decode.sh --nj "$decode_nj" --cmd "$decode_cmd" exp/tri1/graph data/split/$devSplit exp/tri1/decode_dev
fi 

if $tri2_stage; then
  echo ============================================================================
  echo "                 tri2 : LDA + MLLT Training & Decoding                    "
  echo ============================================================================
  # Align te-in-Train_r10k data with tri1 models.
  steps/align_si.sh --nj "$train_nj" --cmd "$train_cmd" data/split/$trainSplit"_r"$tri2Size data/lang exp/tri1 exp/tri1_ali

  # From tri1 system, train tri2 which is LDA + MLLT.
  steps/train_lda_mllt.sh --cmd "$train_cmd" --splice-opts "--left-context=3 --right-context=3" $numLeavesTri2 $numGaussTri2 data/split/$trainSplit"_r"$tri2Size data/lang exp/tri1_ali exp/tri2

  utils/mkgraph.sh data/lang_test_tg exp/tri2 exp/tri2/graph

  steps/decode.sh --nj "$decode_nj" --cmd "$decode_cmd" exp/tri2/graph data/split/$devSplit exp/tri2/decode_dev
fi

if $tri3_stage; then
  echo ============================================================================
  echo "              tri3 : LDA + MLLT + SAT Training & Decoding                 "
  echo ============================================================================
  # Align te-in-Train_r10k data with tri2 models.
  steps/align_si.sh --nj "$train_nj" --cmd "$train_cmd" --use-graphs true data/split/$trainSplit"_r"$tri2Size data/lang exp/tri2 exp/tri2_ali

  # From tri2 system, train tri3 which is LDA + MLLT + SAT.
  steps/train_sat.sh --cmd "$train_cmd" $numLeavesTri3 $numGaussTri3 data/split/$trainSplit"_r"$tri2Size data/lang exp/tri2_ali exp/tri3

  utils/mkgraph.sh data/lang_test_tg exp/tri3 exp/tri3/graph

  steps/decode_fmllr.sh --nj "$decode_nj" --cmd "$decode_cmd" exp/tri3/graph data/split/$devSplit exp/tri3/decode_dev
fi

if $tri3_full_stage; then
  echo ============================================================================
  echo "              tri3 full: LDA + MLLT + SAT Training & Decoding              "
  echo ============================================================================
  # Align full train data with tri3 models.
  steps/align_si.sh --nj "$train_nj" --cmd "$train_cmd" --use-graphs true data/split/$trainSplit data/lang exp/tri3 exp/tri3_ali

  # From tri3 system, train tri3_full which is also LDA + MLLT + SAT.
  steps/train_sat.sh --cmd "$train_cmd" $numLeavesTri3Full $numGaussTri3Full data/split/$trainSplit data/lang exp/tri3_ali exp/tri3_full

  utils/mkgraph.sh data/lang_test_tg exp/tri3_full exp/tri3_full/graph

  steps/decode_fmllr.sh --nj "$decode_nj" --cmd "$decode_cmd" exp/tri3_full/graph data/split/$devSplit exp/tri3_full/decode_dev
fi

